import pymysql
import random
from datetime import datetime, timedelta

# Database Configuration
localhost = "127.0.0.1"
puerto = 3306
username = "root"
passw = "12345678"
db = "EmpresaColectivos"

# Lista de nombres y apellidos aleatorios (puedes agregar más si lo deseas)
nombres = ["Juan", "María", "Luis", "Ana", "Carlos", "Laura"]
apellidos = ["García", "Martínez", "López", "Rodríguez", "Fernández", "Pérez"]

# Establecer conexión con la base de datos
connection = pymysql.connect(host=localhost, port=puerto, user=username, password=passw, database=db)

# Función para generar una fecha aleatoria en un rango de fechas específico
def generar_fecha():
    start_date = datetime.now() - timedelta(days=365)  # Fecha hace un año
    end_date = datetime.now() + timedelta(days=365)  # Fecha dentro de un año
    random_date = start_date + (end_date - start_date) * random.random()
    return random_date.strftime('%Y-%m-%d')

# Función para generar datos aleatorios y realizar inserciones en la base de datos
def generar_datos_aleatorios(cantidad):
    cursor = connection.cursor()
    try:
        for _ in range(cantidad):
            nombre = random.choice(nombres)
            apellido = random.choice(apellidos)
            fecha = generar_fecha()
            ruta = random.choice(['A', 'B'])
            
            # Insertar datos aleatorios en la tabla Clientes
            cursor.execute("INSERT INTO Clientes (Nombre, Apellido, Fecha, Ruta) VALUES (%s, %s, %s, %s)",
                           (nombre, apellido, fecha, ruta))
            connection.commit()
        print(f"Se han generado y insertado {cantidad} registros de clientes de forma aleatoria.")
    except pymysql.Error as e:
        connection.rollback()
        print(f"Error al generar datos aleatorios e insertar en la base de datos: {e}")

# Generar 10 registros de clientes de forma aleatoria (puedes cambiar la cantidad según lo desees)
generar_datos_aleatorios(10)
