import pymysql

# Database Configuration
localhost = "127.0.0.1"
puerto = 3306
username = "root"
passw = "12345678"
db = "EmpresaColectivos"

# Establish the connection
connection = pymysql.connect(host=localhost, port=puerto, user=username, password=passw, database=db)

# Crear base de datos para clientes
cursor = connection.cursor()
try:
    cursor.execute("Insert into Clientes (Nombre, Apellido,Fecha,Ruta) values('Victoria','Kaltakian','2023-12-16','A')")
    connection.commit()
except:
    connection.rollback()