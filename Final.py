import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import hashlib
import mysql.connector
from tkcalendar import Calendar
from datetime import datetime

cal = None

# Configuracion BD
localhost = "127.0.0.1"
puerto = 3306
username = "root"
passw = "12345678"
db = "EmpresaColectivos"

#Funciones BD
def conectar_bd():
    return mysql.connector.connect(
        host=localhost,
        user=username,
        password=passw,
        database=db,
    )

def crear_tabla_empleados():
    conexion = conectar_bd()
    cursor = conexion.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS Empleados (
            id INT AUTO_INCREMENT PRIMARY KEY,
            Usuario VARCHAR(255) NOT NULL,
            Contraseña VARCHAR(64) NOT NULL
        )
    """)
    conexion.commit()
    conexion.close()

def agregar_empleado(usuario, contraseña):
    conexion = conectar_bd()
    cursor = conexion.cursor()
    hashed_contraseña = hash_password(contraseña)
    cursor.execute(
        "INSERT INTO Empleados (Usuario, Contraseña) VALUES (%s, %s)",
        (usuario, hashed_contraseña)
    )
    conexion.commit()
    conexion.close()

def obtener_contraseña_desde_base_de_datos(nombre_usuario):
    conexion = conectar_bd()
    cursor = conexion.cursor()
    cursor.execute("SELECT Contraseña FROM Empleados WHERE Usuario = %s", (nombre_usuario,))
    resultado = cursor.fetchone()
    conexion.close()
    return resultado[0] if resultado else None

# Funcion para encriptar contraseña
def hash_password(password):
    return hashlib.sha256(password.encode('utf-8')).hexdigest()[:64]

# Funcion para crear usuario
def crear_usuario():
    nombre = entry_nombre.get()
    contraseña = entry_contraseña.get()
    if nombre and contraseña:
        agregar_empleado(nombre, contraseña)
        messagebox.showinfo("Éxito", "Empleado creado exitosamente.")
    else:
        messagebox.showwarning("Advertencia", "Por favor, ingresa un nombre de usuario y una contraseña.")

def abrir_ventana_calendario():
    ventana_calendario = tk.Toplevel(root)
    ventana_calendario.title("Selector de Fechas")
    ventana_calendario.geometry("800x600")
    ventana_calendario.configure(bg="white")

    global cal
    cal = Calendar(ventana_calendario, selectmode="day", year=2023, month=12, day=16)
    cal.config(font=('Arial', 20))
    cal.pack(fill="both", expand=True, padx=10, pady=10)  # Expand and fill available space

    btn_get_date = tk.Button(ventana_calendario, text="Mostrar Estadisticas", command=obtener_estadisticas)
    btn_get_date.pack(pady=10)

# Algoritmo de la mochila
def calcular_rentabilidad(viajes_ruta_a, viajes_ruta_b, capacidad_total):
    precio_inicial = 10
    precio_extra = 15

    precio_pasaje_a = precio_inicial if viajes_ruta_a <= 8 else precio_extra
    precio_pasaje_b = precio_inicial if viajes_ruta_b <= 8 else precio_extra

    dp = [0] * (capacidad_total + 1)

    for i in range(1, capacidad_total + 1):
        dp[i] = dp[i - 1]  # Initialize with the previous value
        if viajes_ruta_a + viajes_ruta_b <= i:
            if viajes_ruta_a <= i:  # Check if trips on route A are less than current capacity
                dp[i] = max(dp[i], dp[i - viajes_ruta_a] + (precio_pasaje_a))
            if viajes_ruta_b <= i:  # Check if trips on route B are less than current capacity
                dp[i] = max(dp[i], dp[i - viajes_ruta_b] + (precio_pasaje_b))

    return dp[capacidad_total]

# Algoritmo de la mochila
def calcular_rentabilidad(viajes_ruta_a, capacidad_total):
    precio_inicial = 10
    precio_extra = 15

    ocupacion_maxima = 20  # Capacidad máxima del autobús

    precio_pasaje_a = precio_inicial if viajes_ruta_a <= 8 else precio_extra

    # Usar el algoritmo de la mochila para calcular la rentabilidad según la ocupación
    dp = [0] * (ocupacion_maxima + 1)

    for i in range(1, ocupacion_maxima + 1):
        dp[i] = dp[i - 1]  # Inicializar con el valor previo

        if viajes_ruta_a <= i:
            dp[i] = max(dp[i], dp[i - viajes_ruta_a] + (viajes_ruta_a * precio_pasaje_a))

    rentabilidad = (dp[ocupacion_maxima] / (ocupacion_maxima * precio_extra))*100  # Cálculo de la rentabilidad

    return rentabilidad


def obtener_estadisticas():
    selected_date = cal.get_date()
    formatted_date = datetime.strptime(selected_date, "%m/%d/%y").strftime("%Y-%m-%d")
    print(f"Fecha seleccionada: {formatted_date}")
    viajes_ruta_a, viajes_ruta_b = contar_viajes_por_ruta_en_fecha(formatted_date)
    capacidad_total = 20  # Capacidad total de asientos disponibles

    rentabilidad_a = calcular_rentabilidad(viajes_ruta_a, capacidad_total)
    rentabilidad_b = calcular_rentabilidad(viajes_ruta_b, capacidad_total)

    ventana_estadisticas = tk.Toplevel(root)
    ventana_estadisticas.title("Estadisticas")
    ventana_estadisticas.geometry("800x600")
    ventana_estadisticas.configure(bg="white")

    label_ruta_a = tk.Label(ventana_estadisticas, text=f"Viajes en Ruta A: {viajes_ruta_a}", bg="white", fg="black")
    label_ruta_a.pack(pady=5)
    label_ruta_b = tk.Label(ventana_estadisticas, text=f"Viajes en Ruta B: {viajes_ruta_b}", bg="white", fg="black")
    label_ruta_b.pack(pady=5)

    label_rentabilidad = tk.Label(ventana_estadisticas, text=f"Rentabilidad: {rentabilidad_a}", bg="white", fg="black")
    label_rentabilidad.pack(pady=10)
    label_rentabilidad = tk.Label(ventana_estadisticas, text=f"Rentabilidad: {rentabilidad_b}", bg="white", fg="black")
    label_rentabilidad.pack(pady=10)


def contar_viajes_por_ruta_en_fecha(fecha):
    conexion = conectar_bd()
    cursor = conexion.cursor()

    cursor.execute("SELECT COUNT(*) FROM Clientes WHERE ruta = 'A' AND fecha =%s", (fecha,))
    viajes_ruta_a = cursor.fetchone()[0]

    cursor.execute("SELECT COUNT(*) FROM Clientes WHERE ruta = 'B' AND fecha = %s", (fecha,))
    viajes_ruta_b = cursor.fetchone()[0]

    return viajes_ruta_a, viajes_ruta_b

def iniciar_sesion():
    nombre = entry_nombre.get()
    contraseña = entry_contraseña.get()
    contraseña_almacenada = obtener_contraseña_desde_base_de_datos(nombre)
    if contraseña_almacenada and verificar_contraseña(contraseña, contraseña_almacenada):
        abrir_ventana_calendario()
    else:
        messagebox.showwarning("Advertencia", "Nombre de usuario o contraseña incorrectos.")

def verificar_contraseña(password, hashed):
    return hashed == hash_password(password)

# GUI Funciones pagina principal
root = tk.Tk()
root.geometry("800x600")
root.title("Gestión de Empleados")
root.configure(bg="white")

# Cargar y redimensionar el logo
logo_path = "/Users/martinllado/Downloads/EDP/Final/EcoTravelLogo1.png"  # Replace with your logo path
logo = Image.open(logo_path)
logo.thumbnail((150, 150))
logo = ImageTk.PhotoImage(logo)

logo_label = tk.Label(root, image=logo, bg="white")
logo_label.image = logo
logo_label.grid(row=0, column=0, columnspan=2, padx=10, pady=10, sticky="nsew")

label_nombre = tk.Label(root, text="Usuario:", bg="white", fg="black")
label_nombre.grid(row=1, column=0, padx=10, pady=5)

entry_nombre = tk.Entry(root)
entry_nombre.grid(row=1, column=1, padx=10, pady=5)

label_contraseña = tk.Label(root, text="Contraseña:", bg="white", fg="black")
label_contraseña.grid(row=2, column=0, padx=10, pady=5)

entry_contraseña = tk.Entry(root, show="*")
entry_contraseña.grid(row=2, column=1, padx=10, pady=5)

btn_crear_empleado = tk.Button(root, text="Crear Empleado", bg="white", fg="black", command=crear_usuario)
btn_crear_empleado.grid(row=3, column=0, columnspan=2, padx=10, pady=5)

btn_iniciar_sesion = tk.Button(root, text="Iniciar Sesión", command=iniciar_sesion)
btn_iniciar_sesion.grid(row=4, column=0, columnspan=2, padx=10, pady=5)

root.pack_propagate(False)

logo_label.place(relx=0.5, rely=0.25, anchor="center")

label_nombre.place(relx=0.5, rely=0.5, anchor="center")
entry_nombre.place(relx=0.5, rely=0.55, anchor="center")

label_contraseña.place(relx=0.5, rely=0.65, anchor="center")
entry_contraseña.place(relx=0.5, rely=0.7, anchor="center")

btn_crear_empleado.place(relx=0.5, rely=0.8, anchor="center")
btn_iniciar_sesion.place(relx=0.5, rely=0.85, anchor="center")

# Crear tabla empleados si no existe
crear_tabla_empleados()

# GUI
root.mainloop()
